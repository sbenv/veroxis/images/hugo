FROM registry.gitlab.com/sbenv/veroxis/images/sdks/alpine:3.20.2 as builder

COPY "hugo/hugo" "/usr/local/bin/hugo"
RUN strip "/usr/local/bin/hugo"

# --

FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.20.2

COPY --from=builder "/usr/local/bin/hugo" "/usr/local/bin/hugo"
